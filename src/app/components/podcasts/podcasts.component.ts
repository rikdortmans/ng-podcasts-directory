import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-podcasts',
  templateUrl: './podcasts.component.html',
  styleUrls: ['./podcasts.component.scss']
})
export class PodcastsComponent implements OnInit {

  appName: string = "Testing";
  appVersion: number = 13;
  podcastCategories: string[] = ["Games", "Music", "Stuff"];
  author = {
    name:"Poddy",
    github:"Github",
    avatar:"somelinktoanimage.png"
  }

  constructor() { }

  ngOnInit(): void {
  }

}
